package main

import (
	"html/template"
	"net/http"

	"bitbucket.org/lriveros/cambialo/models"
)

var templates = template.Must(template.ParseGlob("templates/*.tmpl"))

func loadTemplates() {
	for _, t := range templates.Templates() {
		//log.Print(fmt.Sprintf("  # %d -> %s", i, t.Name()))
		templates.Lookup(t.Name())
	}
	//templates.Lookup("busqueda-centro.tmpl.html")
	//templates.Lookup("navbar.tmpl.html")
}

func renderTemplate(w http.ResponseWriter, tmpl string, u *models.User) {
	err := templates.ExecuteTemplate(w, tmpl+".tmpl", u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
