package main

import (
	"log"
	"net/http"

	"bitbucket.org/lriveros/cambialo/models"
)

func loginHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("user_username")
	pass := r.FormValue("user_password")

	if name != "" && pass != "" {
		usuario, err := models.Login(name, encriptarPassword(pass))
		if err == nil {
			setSession(usuario.Nombres, w)
			//	redirectTarget = "/internal"
		} else {
			log.Panic(err)
		}
		renderTemplate(w, "index", usuario)
	} else {
		renderTemplate(w, "index", nil)
	}

}

// logout handler

func logoutHandler(response http.ResponseWriter, request *http.Request) {
	clearSession(response)
	http.Redirect(response, request, "/", 302)
}

func indexPageHandler(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(response, indexPage)
	//http.ServeFile(w, r, "templates/login.tmpl.html")
	//renderTemplate(w, "index", &models.User{ID: 1, Nombres: "Admin", Email: "mail", Username: "addmin", Password: "", Status: 1, Tipo: 1, Created_at: time.Now(), Last_Connection: time.Now()})
	renderTemplate(w, "index", models.NewUser())
}

func personalesHandler(w http.ResponseWriter, r *http.Request) {
	if getUserName(r) != "" {
		user, err := getUser(r)
		if err != nil {
			log.Fatal(err)
			http.Redirect(w, r, "/", 302)
		} else {
			renderTemplate(w, "personales", user)
		}

	} else {
		http.Redirect(w, r, "/", 302)
	}
}
