package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"net/http"
	"os"

	"bitbucket.org/lriveros/cambialo/models"

	"github.com/gorilla/mux"
)

// server main method
func main() {

	port := os.Getenv("PORT")

	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "80"

	}

	cssHandler := http.FileServer(http.Dir("./static/css/"))
	fontsHandler := http.FileServer(http.Dir("./static/fonts/"))
	jsHandler := http.FileServer(http.Dir("./static/js/"))
	imgsHandler := http.FileServer(http.Dir("./static/imgs/"))

	http.Handle("/css/", http.StripPrefix("/css/", cssHandler))
	http.Handle("/fonts/", http.StripPrefix("/fonts/", fontsHandler))
	http.Handle("/js/", http.StripPrefix("/js/", jsHandler))
	http.Handle("/imgs/", http.StripPrefix("/imgs/", imgsHandler))

	loadTemplates()
	models.InitDB()

	var router = mux.NewRouter()
	router.HandleFunc("/", indexPageHandler)

	router.HandleFunc("/login", loginHandler).Methods("POST")
	router.HandleFunc("/logout", logoutHandler).Methods("POST")
	router.HandleFunc("/personales", personalesHandler)

	http.Handle("/", router)
	http.ListenAndServe(":"+port, nil)
}

func encriptarPassword(pass string) string {
	h := md5.New()
	io.WriteString(h, pass)
	password := fmt.Sprintf("%x", h.Sum(nil))
	return password
}
