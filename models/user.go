package models

import (
	"fmt"
	"time"
)

type User struct {
	ID              int
	Nombres         string
	Apellidos       *string //posiblemente nulo
	Imagen          *string //posiblemente nulo
	Email           string
	Username        string
	Password        string
	Status          int
	Tipo            int
	Created_at      time.Time
	Last_Connection time.Time
}

func NewUser() *User {
	return &User{}
}

/**Login de suaurio**/
func Login(username string, password string) (*User, error) {
	query := fmt.Sprintf(queryLogin, username, username, password)
	rows, err := db.Query(query)
	user := new(User)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	//usuario encontrado
	for rows.Next() {
		usr := new(User)
		//id, nombres, apellidos, imagen, email, username, status, tipo, created_at, last_connection
		err := rows.Scan(&usr.ID, &usr.Nombres, &usr.Apellidos, &usr.Imagen, &usr.Email, &usr.Username, &usr.Status, &usr.Tipo, &usr.Created_at, &usr.Last_Connection)
		if err != nil {
			return nil, err
		}
		user = usr
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	//outgoingJSON, _ := json.Marshal(user)
	//log.Print(fmt.Sprintf("Retornando al usuario %s", string(outgoingJSON)))
	return user, nil
}

/*
func AllUsers() ([]*User, error) {
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*User, 0)
	for rows.Next() {
		usr := new(Book)
		err := rows.Scan(&usr.Isbn, &usr.Title, &usr.Author, &usr.Price)
		if err != nil {
			return nil, err
		}
		usr = append(users, usr)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return users, nil
}
*/
