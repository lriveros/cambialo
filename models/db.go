package models

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/lib/pq"
)

var db *sql.DB

func InitDB() {
	var err error
	//log.Printf("Conectar con %s", os.Getenv("DATABASE_URL"))
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	//conectar a bd
	if err != nil {
		log.Panic(err)
	}
	//ping a bd
	if err = db.Ping(); err != nil {
		log.Panic(err)
	}
}
